## moCronik's Server Side Drivable Wheelchair

## Adds in a drivable wheelchiar for 7 Days to Die.

This mod pack makes it so you can pick up the current in game wheelchairs as a variant helper block which you can place down in any color available in the game or use as a part to make the drivable wheelchair!

Craft in the workbench the drivable wheelchair with some parts found in the world! Hint to find wheelchairs in the wild, look for the Medic buildings.

I was paralyzed in 2009 from an attempted suicide motorcycle accident and have struggled to get back to normal life. Gaming is one way to keep me happy and hopeful even with a painful and difficult life now.

So I have gotten into modding and have added a drivable wheelchair and my logo into the game, so my next project was a vanilla drivable wheelchair. After some work I have finished and ready to release to the world.

## Installation
Download the "VanillaDrivableWheelchiar.zip" file and extract the files into your 7 Days to Die /Mods folder. If no /Mods folder than just create one and place the files in there.

Single player and server side only for multiplayer, so only the server needs the mod installed.

## Usage
Free to use. No modification or using in other mod packs without consent.

## Support
Contact me for help in my Discord at: https://discord.gg/8BGJK2pte7

## Support my content creation:
Merch: https://store.streamelements.com/mocronik-1942

Tips: https://streamelements.com/mocronik-8842/tip

## Follow me at:
YouTube: https://www.youtube.com/@mocronik

Discord: https://discord.gg/8BGJK2pte7

Twitter: https://twitter.com/moCronik

Steam: 50766738

## Roadmap
Basicly finished but might re work the bike model to the 4x4 one later.

## Authors and acknowledgment
moCronik

## License
Open source License  *will update soon

## Changelog

v1.1.420: Small patch to fix craft time and added the craftable tag to the recipe file.

Inital v1.0.420: Release on 4/29/24 to full version to share with others.
